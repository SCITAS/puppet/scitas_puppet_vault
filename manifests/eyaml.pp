class scitas_puppet_vault::eyaml (
  Stdlib::HTTPUrl     $uri,
  String              $cert_path,
  Array[String]       $paths,
  Stdlib::Unixpath    $hiera_vault_path_file = "${::puppet_environmentpath}/${::environment}/data/vault.eyaml"
) {

  assert_private()

  if $::scitas_puppet_vault::generate_eyaml {
    $hash = vault_get_hash($uri,
              $cert_path,
              $paths,
              true)
    if $hash.empty {
      warning('Unable to return any data. Check parameters and connexion with Vault.')
    }
    else {
      file { $hiera_vault_path_file :
        ensure  => 'file',
        content => vault_eyaml_encrypt($hash).to_yaml(),
        mode    => '0400',
      }
    }
  }
}
