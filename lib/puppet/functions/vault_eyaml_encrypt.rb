Puppet::Functions.create_function(:vault_eyaml_encrypt) do
  # @param data String containing the data to encrypt
  # @return [String] Contains the data encrypt with the pkcs7 public key (cert) from the puppet client
  dispatch :vault_encrypt do
    param 'Hash', :data
  end

  def vault_encrypt(data)
    Puppet.debug("'Vault PKCS7 encrypt'")
    parse_hash(data)
  end

  def parse_hash(hash)
    hash.transform_values do |v|
      case v
      when Hash
        parse_hash(v)
      else
        ScitasVault.encrypt_pkcs7(v)
      end
    end
  end
end
