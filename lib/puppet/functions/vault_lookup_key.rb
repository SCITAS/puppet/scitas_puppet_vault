# rubocop:disable Metrics/MethodLength, Metrics/BlockLength
require 'vault'
require "#{File.dirname(__FILE__)}/../../puppet_x/vault/vault.rb"

Puppet::Functions.create_function(:vault_lookup_key) do
  dispatch :vault_lookup_key do
    param 'String[1]', :key
    param 'Hash[String[1],Any]', :options
    param 'Puppet::LookupContext', :context
  end

  def vault_lookup_key(key, options, context)
    return context.cached_value(key) if context.cache_has_key(key)

    # Can't do this with an argument_mismatch dispatcher since there is no way to declare a struct that at least
    # contains some keys but may contain other arbitrary keys.
    unless options.include?('paths') && options.include?('uri')
      raise ArgumentError,
            _("'vault_lookup_key': 'uri' and 'paths' must be declared in hiera.yaml " \
              'when using this lookup_key function')
    end

    # nil key value is a Boolean and it is used to indicate that the cache contains all the content of the Vault paths
    if !context.cached_value(nil) || context.cached_value(nil).nil?
      raw_data = load_data_hash(options, context)
      context.cache_all(raw_data)
      context.cache(nil, true)
    end
    context.not_found unless context.cache_has_key(key)
  end

  def load_data_hash(options, context)
    begin
      ScitasVault.init(options['uri'], options['auth_path'], options['ca_path'], options['verify_ssl'])
      res = ScitasVault.read_paths(options['paths'], options['version'])
      res = Puppet::Pops::Lookup::HieraConfig.symkeys_to_string(res)
      res = ScitasVault.neste_hash(res)
    rescue Vault::VaultError
      Puppet.warning("Unable to connect to Vault engine(s) : #{ScitasVault.address}")
      res = {}
    end
    context.interpolate(res)
  end
end
# rubocop:enable Metrics/MethodLength, Metrics/BlockLength
