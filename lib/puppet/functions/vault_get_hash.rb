# rubocop:disable Metrics/ParameterLists, Style/OptionalBooleanParameter
require 'vault'
require "#{File.dirname(__FILE__)}/../../puppet_x/vault/vault.rb"
Puppet::Functions.create_function(:vault_get_hash) do
  # @param vault_uri The full URI to the Vault API endpoint for a key/value secrets engine path.
  # @param auth_path The Vault mount path of the 'cert' authentication type used with Puppet certificates.
  # @param path The Vault mount path of the k/v secret engine
  # @param version Set this value to 'v2' to use version 2 of the Vault key/value secrets engine.
  # @param timeout Value in seconds to wait for Vault connections.
  # @param ca_trust The path to the trusted certificate authority chain file. Some OS defaults will be attempted if nil.
  # @return [Hash] Contains all the key/value pairs from the given path.
  dispatch :get_hash do
    param 'String', :vault_uri
    param 'String', :auth_path
    param 'Array[String]', :paths
    optional_param 'Boolean', :ssl_verify
    optional_param 'String', :ca_trust
    optional_param 'Integer', :timeout
  end

  def get_hash(vault_uri, auth_path, paths, ca_trust = '', version = 'v2', ssl_verify = false)
    Puppet.debug("'Vault get merge hash from paths'")

    ScitasVault.init(vault_uri, auth_path, ca_trust, ssl_verify)
    begin
      res = ScitasVault.read_paths(paths, version)
    # 'rescue Vault::VaultError' is not working and I do not know why
    rescue StandardError
      Puppet.warning("Unable to connect to Vault engine(s) : #{ScitasVault.address}")
      {}
    end
    res = Puppet::Pops::Lookup::HieraConfig.symkeys_to_string(res)
    ScitasVault.neste_hash(res)
  end
end
# rubocop:enable Metrics/ParameterLists, Style/OptionalBooleanParameter
