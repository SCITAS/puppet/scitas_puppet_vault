# rubocop:disable Metrics/MethodLength, Metrics/ModuleLength, Metrics/AbcSize, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity, Style/OptionalBooleanParameter

require 'vault'

# ScitasVault for puppet
module ScitasVault
  attr_reader :vault

  class << self
    def setup!
      @vault = Vault
      @vault.setup!
      cert = File.read(Puppet.settings[:hostcert])
      priv = File.read(Puppet.settings[:hostprivkey])
      @vault.ssl_pem_contents = cert + priv
      # @vault.ssl_ca_path = Puppet.settings[:localcacert]
      @public_key_x509 = OpenSSL::X509::Certificate.new(cert)
      @cipher = OpenSSL::Cipher.new('aes-256-cbc')
      @go = true
    rescue StandardError
      Puppet.warning('Missing or incorrect SSL certificate')
      @go = false
    end

    def init(uri, auth_path, ca_path = '', ssl_verify = true)
      @vault.ssl_verify = ssl_verify
      @vault.address = uri
      @vault.ssl_ca_path = ca_file(ca_path)
      # jruby from puppetserver only use TLSv1/SSLv3 ciphers
      # by default Vault client use TLSv1.2 ciphers
      # If we find a way to force TLSv1.2+ ciphers to jruby we can remove this setting
      @vault.ssl_ciphers = 'TLSV1.3:TLSv1.2:TLSv1:!aNULL:!eNULL'
      # check if token is expired
      if @go
        begin
          Vault.auth_token.lookup_self
        rescue StandardError => e
          raise(e) unless e.class.name.start_with?('Vault::')

          @go = false
        end
      end
      return if @go

      begin
        data = @vault.post("/v1/auth/#{auth_path.delete_suffix('/')}/login")
        @vault.token = data[:auth][:client_token]
        @go = true
      rescue StandardError
        Puppet.warning("Unable to acquire a token for Vault #{uri}")
        @go = false
      end
    end

    def encrypt_pkcs7(value)
      # @summary Encrypt a value with the puppet certificate
      # @param [Symbol] :value The value to encrypt
      # @return [String] The value encrypted and convert to base64 for storage
      enc = OpenSSL::PKCS7.encrypt([@public_key_x509], value.to_s, @cipher, OpenSSL::PKCS7::BINARY).to_der
      enc = Base64.strict_encode64(enc)
      "ENC[PKCS7,#{enc}"
    end

    def str2array(value)
      # @summary Convert a string reprensenting a yaml array into a ruby array
      # @param [Type unknown] : value
      if value.instance_of? String
        value.gsub!(/(,)(\S)/, '\\1 \\2')
        begin
          value = YAML.safe_load(value)
        rescue Psych::SyntaxError
          Puppet.warning("Invalid value : #{value} is not an array ")
          value = []
        end
      end
      # return empty array if YAML::load return a string
      # or param(value) is neither an array nor a string
      value.instance_of?(Array) ? value : []
    end

    def str2hash(value)
      # @summary Convert a string reprensenting a json into a ruby array
      # @param [Type unknown] : value
      if value.instance_of? String
        value = begin
          JSON.parse(value)
        rescue StandardError
          value
        end
      end
      value
    end

    def read_paths(paths, version = 'v2')
      # @summary Grab all k/v from multiple Vault paths
      # @param [Array[String]] :paths A list of paths to parse
      # @param [String] :version The version of the k/v paths ('v1' or 'v2'), by default 'v2'
      return unless @go

      paths = str2array(paths)
      res = {}
      paths.each do |path|
        if version == 'v2'
          arr = path.split('/')
          path = "#{arr[0]}/data/#{arr[1]}" unless arr.length != 2
          begin
            data = Vault.logical.read(path)
          rescue Vault::VaultError
            Puppet.warning("Unable to read Vault path '#{path}'")
            data = nil
          end
          temp = data.nil? ? {} : data.data[:data]
          res = res.merge(temp)
        else
          res = res.merge(Vault.logical.read(path))
        end
      end
      res
    end

    def neste_hash(hash)
      # @summary Transform a flat hash with the key.subkey notation into a nested one
      # For example {module1::key1.subkey1 => "1", module1::key1.subkey1 => "2", module2::key1 => "11"}  would become
      # {module1::key1 => {subkey1 => "1", subkey2 => "2"}, module2::key1 => "11"}
      # @param [Hash] :hash The flat hash
      # @return [Hash] A nested hash
      return {} if hash.nil?

      res = {}

      hash.each do |key, value|
        h = res

        parts = key.to_s.split('.')
        while parts.length.positive?
          new_key = parts[0]
          rest = parts[1..]
          unless h.instance_of?(Hash) || (h.instance_of?(Array) && (new_key.to_s == new_key.to_i.to_s))
            raise ArgumentError, "Trying to set key #{new_key} to value #{value} on a non hash/array #{h}\n"
          end

          new_key = new_key.to_i if new_key.to_s == new_key.to_i.to_s
          if rest.empty?
            if h[new_key].instance_of?(Hash) || h[new_key].instance_of?(Array)
              raise ArgumentError,
                    'Replacing a hash or an array with a scalar.' \
                    "key #{new_key}, value #{value}, current value #{h[new_key]}\n"
            end

            h.store(new_key, str2hash(value))
            break
          end
          h[new_key] = rest[0].to_s == rest[0].to_i.to_s ? [] : {} if h[new_key].nil?
          h = h[new_key]
          parts = rest
        end
      end

      res
    end

    def ca_file(ca_path)
      # @summary Try known paths for trusted CA certificates when not specified
      # @param [String] :ca_path The path to a trusted certificate authority file. If nil, some defaults are attempted.
      # @return [String] The verified file path to a trusted certificate authority file.
      ca_file = if ca_path && File.exist?(ca_path)
                  ca_path
                elsif File.exist?('/etc/pki/ca-path/extracted/pem/tls-ca-bundle.pem')
                  '/etc/pki/ca-path/extracted/pem/tls-ca-bundle.pem'
                elsif File.exist?('/etc/ssl/certs/ca-certificates.crt')
                  '/etc/ssl/certs/ca-certificates.crt'
                elsif File.exist?('/etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt')
                  '/etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt'
                end
      raise Puppet::Error, 'Failed to get the pathed CA certificate file.' if ca_file.nil?

      ca_file
    end

    # Delegate all methods to the client object, essentially making the module
    # object behave like a {Client}.
    def method_missing(mmm, *args, &block)
      if @vault.respond_to?(mmm)
        @vault.send(mmm, *args, &block)
      else
        super
      end
    end

    # Delegating +respond_to+ to the {Client}.
    def respond_to_missing?(mmm, include_private = false)
      @vault.respond_to?(mmm, include_private) || super
    end
  end
end
ScitasVault.setup!
# rubocop:enable Metrics/MethodLength, Metrics/ModuleLength, Metrics/AbcSize, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity, Style/OptionalBooleanParameter
