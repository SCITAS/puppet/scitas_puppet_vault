# Munge

## Module Description

This module allows you to grab all available data from path in Vault k/V engine instances and put it in a eyaml file for future use.

It provides functions to :
* create a short lived token using puppet certificate and key from the node to access Vault path
* read path(s) from Vault using the short lived token
* grab all data from Vault path(s) and put it in a eyaml file encrypted with puppet certificate from the node

## Setup

### Setup Requirements
The module depends on:

* `vault-ruby` for secret management (can be found here: https://github.com/hashicorp/vault-ruby).
  * Vault is a tool for securely accessing secrets (https://github.com/hashicorp/vault)
  * The module lookup for scitas_puppet_vault::* values for Vault access (uri, path_certs, paths, ssl_verify, ca_trust)

### Beginning with Vault

<TODO>

## Usage

You can use the module by simply including the main class:

```
 node default {
   include scitas_puppet_vault
 }
```

 You can also apply the main class to the local system using puppet apply:


```
 cd $(puppet config print modulepath | awk -F : '{print $1}')
 git clone https://c4science.ch/source/scitas_puppet_vault.git
 puppet apply -e 'include scitas_puppet_vault'

```

## Limitations

This module is mainly tested on RHEL v8.

## Development

Patches and issues can be submitted on:
https://c4science.ch/source/scitas_puppet_vault/

## Contributors

The list of contributors can be found at file [AUTHORS.md](https://c4science.ch/source/scitas_puppet_vault/browse/master/AUTHORS.)
