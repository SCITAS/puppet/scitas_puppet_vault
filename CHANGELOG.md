# Changelog
All notable changes to this project will be documented in this file.
These should not affect the functionality of the module.

## VERSION DATE FIRST NAME LAST NAME <E-MAIL>

* Initial release
