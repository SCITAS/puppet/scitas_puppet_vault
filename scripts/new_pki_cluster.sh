CLUSTER=$1
TMP=/tmp/acl.hcl


#POLICY
vault policy read acl_foreman > $TMP
cat << EOF >> $TMP
path "pki_$CLUSTER/*" {

   capabilities = ["read", "update", "list", "delete" ]

}
EOF

vault policy write acl_foreman $TMP

#APPROLE for foreman connexion
#Already created, the value are in approle_id secret
# vault write auth/approle/role/role_foreman policies="acl_foreman"
# vault read auth/approle/role/role_foreman/role-id
# vault write -f auth/approle/role/role_foreman/secret-id

#PKI SECRET
vault secrets enable -path=pki_"$CLUSTER" pki
vault secrets tune -max-lease-ttl=87600h pki_"$CLUSTER"
vault write -field=certificate pki_"$CLUSTER"/root/generate/internal \
     common_name=""$CLUSTER".cluster" \
     issuer_name=""$CLUSTER"" \
     ttl=87600h > "$CLUSTER"_ca.crt
vault write pki_"$CLUSTER"/roles/"$CLUSTER" \
     allowed_domains=""$CLUSTER".cluster,scitas-internal-"${CLUSTER::1}"smartproxy.epfl.ch" \
     allow_subdomains=true \
     allow_bare_domains=true \
     max_ttl="43800h"

#AUTH using  PKI SECRET with custom ACL
vault auth enable -path puppet-pki cert

cat << EOF > $TMP
path "clusters/data/$CLUSTER" {

  capabilities = ["read"]

}
EOF
vault policy write "cluster_$CLUSTER" $TMP
vault write auth/puppet-pki/certs/"$CLUSTER" \
    display_name="$CLUSTER" \
    policies=cluster_"$CLUSTER",cluster_common \
    certificate=@"$CLUSTER"_ca.crt\
    ttl=300
